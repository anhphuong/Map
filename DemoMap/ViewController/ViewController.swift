//
//  ViewController.swift
//  DemoMap
//
//  Created by kien on 03/08/2018.
//  Copyright © 2018 kien. All rights reserved.
//

import UIKit

class ViewController: UIViewController, FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
   
    
   
    
    // id gg : 774345153053-dnet8vkso64noev66u2dmecddd2895rp.apps.googleusercontent.com
//AIzaSyBEWbxEOVr4rbx6O4Hqa7Fv0MddoLQkgks
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var viewInfor: UIView!
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var passTf: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var viewLoginFb: UIView!
    var emailUserFb : String?
    let emailDefault = "anhphuong@gmail.com"
    let passDefault = "12345678"
   let loginFB = FBSDKLoginButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
        loginFB.readPermissions = ["email"]
        loginFB.delegate = self
        loginFB.center = view.center
        loginFB.translatesAutoresizingMaskIntoConstraints = false
        self.viewLoginFb.addSubview(loginFB)
        customLayoutForLoginFBButton()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        emailTf.text = ""
        passTf.text = "" 
        if let _ = FBSDKAccessToken.current()
        {
            fetchUserProfile()
        }
    }
    func customLayoutForLoginFBButton() {
        let top = NSLayoutConstraint(item: loginFB, attribute: .top, relatedBy: .equal, toItem: viewLoginFb, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: viewLoginFb, attribute: .bottom, relatedBy: .equal, toItem: loginFB, attribute: .bottom, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: loginFB, attribute: .leading, relatedBy: .equal, toItem: viewLoginFb, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: viewLoginFb, attribute: .trailing, relatedBy: .equal, toItem: loginFB, attribute: .trailing, multiplier: 1, constant: 0)
        viewLoginFb.addConstraints([top, bottom, trailing, leading])
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        nextMap()
    }
    func initView() {
        viewInfor.layer.cornerRadius = 10;
        viewInfor.layer.masksToBounds = true;
        view1.layer.borderColor = UIColor.black.cgColor
        view1.layer.borderWidth = 0.25
        view2.layer.borderColor = UIColor.black.cgColor
        view2.layer.borderWidth = 0.25
    }
    
    func fetchUserProfile() {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, email, name"])
        graphRequest.start { (connection, result, error) in
            if error != nil {
                return
            }
            guard let userInfo = result as? [String: Any] else {return}
            if let email = userInfo["email"] as? String {
                self.emailTf.text = email
                self.emailUserFb = email
            }
        }

    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print("error")
        }else if result.isCancelled {
            print("cancel")
        }else {
            nextMap()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        emailTf.text = ""
        emailUserFb = ""
    }
    
    @IBAction func nextMapVC(_ sender: Any) {
     //   let mapVC : MapViewController = MapViewController()
        if emailUserFb != nil || (emailTf.text == emailDefault && passTf.text == passDefault) {
             nextMap()
        }else {
           let alert = UIAlertController(title: "Sign In Fail", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
            
        }
     
       
    }
    func nextMap() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let mh = sb.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.present(mh, animated: true, completion: nil)
    }
    
}

