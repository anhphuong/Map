//
//  MapViewController.swift
//  DemoMap
//
//  Created by kien on 03/08/2018.
//  Copyright © 2018 kien. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
class MapViewController: UIViewController, CLLocationManagerDelegate , UISearchBarDelegate,  GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate{
 
    
    
    
    
    @IBOutlet weak var map: UIView!
    var mapView : GMSMapView!
    var locationManager = CLLocationManager()
    var resultsArray = [String]()
    var minemarker: GMSMarker!
    var findermarker: GMSMarker!
 
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func viewWillAppear(_ animated: Bool) {
     
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        initGoogleMap()

    }
    func initGoogleMap() {
        if mapView != nil {
            return
        }
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 17.0)
        self.mapView = GMSMapView.map(withFrame: self.map.frame, camera: camera)
        self.mapView.delegate = self
        self.mapView.settings.myLocationButton = true
        self.mapView.isMyLocationEnabled = true
        self.view.addSubview(mapView)
        
    }
    

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        self.mapView.camera = camera
        if minemarker == nil {
            minemarker = GMSMarker()
            minemarker.title = "I'm here "
            minemarker.map = self.mapView
        }
        minemarker.position = CLLocationCoordinate2D(latitude:(location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
        self.locationManager.stopUpdatingLocation()

    }
    // GMSMapview Delegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.mapView.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.mapView?.isMyLocationEnabled = true
        if gesture{
            mapView.selectedMarker = nil
        }
    }
    
    
    @IBAction func backView(_ sender: Any) {
        dismiss(animated: true , completion: nil)
    }
    
    @IBAction func searchAdress(_ sender: Any) {

        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        self.locationManager.startUpdatingLocation()
        self.present(autoCompleteController, animated: true, completion:  nil)
        
    }
    func getMarker(position: CLLocationCoordinate2D, mapView: GMSMapView) {
        
        if findermarker != nil {
            findermarker.map = nil
            findermarker = nil

        }
        findermarker = GMSMarker()
        findermarker.title = "I'm here "
        findermarker.map = self.mapView
        
        findermarker.position = CLLocationCoordinate2D(latitude: position.latitude, longitude: position.longitude)
        let camera = GMSCameraPosition.camera(withTarget: findermarker.position, zoom: 17.0)
        mapView.camera = camera
    }
    // autocompleteController
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15)
        self.mapView.camera = camera
        self.getMarker(position: place.coordinate, mapView: self.mapView)
        self.dismiss(animated: true, completion: nil)
       
      
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR: \(error)")
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    

}
